file(GLOB_RECURSE sources CONFIGURE_DEPENDS
    src/*.c
    src/*.h
    include/*.h
)

add_executable(image-transformer ${sources} "include/from_to_bmp.h" "include/header_bmp.h" "include/image_rotate.h" "include/work_with_file.h" "include/work_with_image.h" "src/bmp_wtite_read.c" "src/file.c" "src/image_create_delete.c" "src/rotate.c")
target_include_directories(image-transformer PRIVATE src include)
