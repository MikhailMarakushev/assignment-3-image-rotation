#ifndef FROM_TO_BMP_H
#define FROM_TO_BMP_H
#include "work_with_image.h"
#include <stdio.h>

enum read_status {
	READ_BMP_OK = 0,
	READ_SIGNATURE_ERROR,
	READ_BITS_ERROR,
	READ_HEADER_ERROR,
	READ_FILE_SIZE_ERROR,
	READ_IMAGE_SIZE_ERROR,
	READ_ERROR

};

enum  write_status {
	WRITE_OK = 0,
	WRITE_ERROR
};

enum read_status from_bmp(FILE* in, struct image* image);

enum write_status to_bmp(FILE* out, struct image* image);



#endif // !FROM_TO_BMP_H
