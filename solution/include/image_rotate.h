#ifndef IMAGE_ROTATE_H
#define IMAGE_ROTATE_H


struct image rotate(struct image const source);

#endif // !IMAGE_ROTATE_H
