#ifndef WORK_WITH_IMAGE_H
#define WORK_WITH_IMAGE_H
#include <inttypes.h>
#include <stddef.h>
#include <stdlib.h>


//for image
#pragma pack(push, 1)
struct image {
	uint32_t width;
	uint32_t height;
	struct pixel* data;
};
#pragma pack(pop)

//for pixel
#pragma pack(push, 1)
struct pixel {
	uint8_t b, g, r;
};
#pragma pack(pop)


#endif 
