#ifndef WORK_WITH_FILE_H
#define WORK_WITH_FILE_H
#include <stdio.h>

enum state {
	SUCCESS = 0,
	OPEN_FAIL,
	CLOSE_FAIL,
};

enum state open_file(FILE** f, char const* path, char const* mode);

enum state close_file(FILE* f);


#endif // !WORK_WITH_FILE_H
