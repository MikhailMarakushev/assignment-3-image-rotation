#ifndef HEADER_BMP_H
#define HEADER_BMP_H

#define BMP_BF_TYPE 0x4d42
#define BMP_TOTAL_BIT_COUNT 24
#define BMP_RESERVED 0
#define BMP_BYTE_HEADER_SIZE 40
#define BMP_BI_PLANES 1
#define BMP_RGB_COMPRESSION 0
#define UNUSED 0

#include  <stdint.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)


#endif
