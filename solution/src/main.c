#include "from_to_bmp.h"
#include "image_rotate.h"
#include "work_with_file.h"
#include "work_with_image.h"
#include <stdio.h>
#include <stdlib.h>

void img_delete(struct image image);

int main(int argc, char** argv) {
    enum state open_file_status;
    struct image image;

    fprintf(stderr,"The format for this program is: '%s \"path to input input_img\" \"path to output input_img\".\n", argv[0]);
    if (argc != 3) {
        printf("You pasd wrong arg");
        return 1;
    }
    

    //open files to read and write
    FILE** input_f = malloc(sizeof(FILE*));
    FILE** output_f = malloc(sizeof(FILE*));
    open_file_status = open_file(input_f, argv[1], "rb");
    if (open_file_status != SUCCESS) {
        printf("Error in opening the input file");
        free(input_f);
        free(output_f);
        return 1;
    }

    
    from_bmp(*input_f, &image);
    
    


    
    if (close_file(*input_f) == CLOSE_FAIL) {
        printf("File not closed");
        free(input_f);
        free(output_f);
        img_delete(image);
        return -1;
    }

    struct image rotated_img = rotate(image);
    if (rotated_img.data == NULL) {
        free(input_f);
        free(output_f);
        img_delete(image);
        img_delete(rotated_img);
        printf("Rotate fail");
        return -2;
    }

    img_delete(image);




    if (open_file(output_f, argv[2], "wb") != SUCCESS) {
        img_delete(rotated_img);
        printf("Error output file");
        free(input_f);
        free(output_f);
        return -3;
    }

    if (to_bmp(*output_f, &rotated_img) != WRITE_OK) {
        free(input_f);
        free(output_f);
        printf("Error in write rotated image");
        img_delete(rotated_img);
        return -4;
    }


    img_delete(rotated_img);

    if (close_file(*output_f) == CLOSE_FAIL) {
        printf("error closing");
    }

    free(input_f);
    free(output_f);





    return 0;
}
