#include "work_with_file.h"
#include <stddef.h>
#include <stdio.h>

enum state open_file(FILE** file, const char* pathname, const char* mode) {
    *file = fopen(pathname, mode);
    return *file == NULL ? OPEN_FAIL : SUCCESS;
}

enum state close_file(FILE* f) {
    return fclose(f) == 0 ? SUCCESS : CLOSE_FAIL;
}
