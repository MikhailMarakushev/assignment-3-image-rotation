#include "from_to_bmp.h"
#include "header_bmp.h"
#include "work_with_image.h"

#include <inttypes.h>
#include <stdio.h>

struct image img_create(uint32_t width, uint32_t height);


uint32_t file_size_c(uint32_t img_size) {
    return (img_size + sizeof(struct bmp_header));
}


enum read_status from_bmp(FILE* in, struct image* image) {
    struct bmp_header bmpHeader;


    if (!fread(&bmpHeader, sizeof(struct bmp_header), 1, in)) {
        printf("1");
        return READ_HEADER_ERROR;
    }


    if (bmpHeader.bfType != BMP_BF_TYPE) {
        printf("2");
        return READ_SIGNATURE_ERROR;
    }

    if (bmpHeader.biBitCount != BMP_TOTAL_BIT_COUNT) {
        printf("3");
        return READ_BITS_ERROR;
    }


    *image = img_create(bmpHeader.biWidth, bmpHeader.biHeight);

    if (image->data == NULL) {
        printf("4");
        return READ_IMAGE_SIZE_ERROR;
    }

    uint32_t padding = (4 - (bmpHeader.biWidth * 3) % 4) % 4;


    for (uint64_t i = 0; i < bmpHeader.biHeight; i++) {
        

        if (fread((image->data) + bmpHeader.biWidth * i, sizeof(struct pixel) * bmpHeader.biWidth, 1, in) != 1) {
            return READ_ERROR;
        }

        if (fseek(in, padding, SEEK_CUR)) {
            return READ_ERROR;
        }

    }

    return READ_BMP_OK;

}


enum write_status to_bmp(FILE* out, struct image* image) {
    struct bmp_header out_bmpHeader;
    uint32_t padding = (4 - (image->width * 3) % 4) % 4;

    const uint32_t image_size = (image->width + padding) * sizeof(struct pixel) * (image->height);
    
    const uint32_t file_size = file_size_c(image_size);


    out_bmpHeader = (struct bmp_header){
        .bfType = BMP_BF_TYPE,
        .bfileSize = file_size,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BMP_BYTE_HEADER_SIZE,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = 1,
        .biBitCount = BMP_TOTAL_BIT_COUNT,
        .biCompression = BMP_RGB_COMPRESSION,
        .biSizeImage = image_size,
        .biXPelsPerMeter = UNUSED,
        .biYPelsPerMeter = UNUSED,
        .biClrUsed = UNUSED,
        .biClrImportant = UNUSED
    };

    if (fwrite(&out_bmpHeader, sizeof(struct bmp_header), 1, out) == 0) {
        return WRITE_ERROR;
    }



    for (uint64_t i = 0; i < image->height; i++) {
       

        if (fwrite(image->data + image->width * i, sizeof(struct pixel), image->width, out) == 0) {
            return WRITE_ERROR;
        }
        uint8_t empty_b[3] = { 0 };

        if ((!fwrite(&empty_b, padding, 1, out)) && (padding != 0)) {
            return WRITE_ERROR;
        }

    }
    return WRITE_OK;




}








