#include "image_rotate.h"
#include "work_with_image.h"
#include <stdint.h>
struct image img_create(uint32_t width, uint32_t height);

struct image rotate(struct image const source) {
    struct image rotated_image = img_create(source.height, source.width);

    if (rotated_image.data == NULL) return rotated_image;

    for (size_t i = 0; i < source.width; i++) {
        for (size_t j = 0; j < source.height; j++) {
            rotated_image.data[(i * source.height) + j] = source.data[i + (source.height - 1 - j) * source.width];
        }
    }
    return rotated_image;
}

