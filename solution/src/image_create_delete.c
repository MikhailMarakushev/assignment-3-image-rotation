#include "work_with_image.h"

struct image img_create(uint32_t width, uint32_t height) {
    struct pixel* data = malloc(sizeof(struct pixel) * (width * height));
    return (struct image) { .width = width, .height = height, .data = data };
}


void img_delete(struct image image) {
    free(image.data);
    image.height = 0;
    image.width = 0;
}
